import React, { FC } from 'react';

import AppBar from '@material-ui/core/AppBar';
import { IconButton, makeStyles, Theme, Toolbar, Typography } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

type MenuProps = {
    appName: string
}

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
    },
    appBar: {
        marginBottom: "16px"
    }
}));

export const MenuBar: FC<MenuProps> = ({ appName }) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="relative" className={classes.appBar}>
                <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="menu">
                        <MenuIcon />
                    </IconButton>

                    <Typography variant="h6">
                        {appName}
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
}