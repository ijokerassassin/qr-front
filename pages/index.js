import Head from 'next/head'
import { appName } from '../config/app.ts';
import { MenuBar } from '../components/layout/header';

import Container from '@material-ui/core/Container';
import dynamic from 'next/dynamic'
import { useState } from 'react';
import { Card, CssBaseline, Grid } from '@material-ui/core';

const QrReader = dynamic(() => import('react-qr-reader'), {
    ssr: false
});

export default function Home() {
  const [result, setResult] = useState("No result!");

  const handleScan = data => {
    if (data) {
      setResult(data);
    }
  }

  const handleError = err => {
    console.error(err)
  }

  return (
    <>
      <CssBaseline/>
      <MenuBar appName={appName} />

      <Container>
        <Grid item xs={12}>
          <Card>
            <QrReader
              delay={300}
              onError={handleError()}
              onScan={handleScan()}
              style={{ width: '100%' }}
            />
            <p>{result}</p>
          </Card>
        </Grid>

        <footer>
            Powered by Kasper de Boer
        </footer>
      </Container>
    </>
  )
}
